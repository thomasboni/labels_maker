# Labels maker

This simple script permit to automatically create Gitlab labels using Gitlab
API. It's an adaptation of a script by @wilfriedroset.

## Usage

```shell
labels_maker.py [-h] [-v] [-d] [-q] -f FILE [-g] [--delete] -H HOST -t TOKEN --id ID [--http] [-k] [-u]

General arguments:
  -h, --help            show this help message and exit
  -v, --verbose         verbose mode
  -d, --debug           debug mode
  -q, --quiet           show only ERROR & CRITICAL messages
  -f FILE, --file FILE  input file containing data aboout labels to create
  -g, --group           create labels at group level instead of project
  --delete              WARNING: this option deletes all existing labels and exit

Gitlab arguments:
  options about gitlab server & project

  -H HOST, --host HOST  gitlab server adress
  -t TOKEN, --token TOKEN
                        api token
  --id ID               id of the project/group
  --http                this option permit to use HTTP instead of HTTPS to reach gitlab server
  -k, --insecure        this option explicitly allows to perform 'insecure' SSL connections and transfers.
  -u, --update          update the label color if its already exists
```

## Labels file

You have to provide a json file as `-f` argument. This file must contains
labels you want to create in following fomat:

```yaml
[
 {
   "name": "bug",
   "color": "8E44AD",
   "description": "bug issue"
 },
 {
   "name": "security",
   "color": "8E44AD",
   "description": "security issue"
 }
]
```

You can use the example file `labels.example.json` to start your own labels
configuration:

```shell
cp labels.example.json labels.my_project.json
```
