#!/usr/bin/python

"""
Create labels in a gitlab project or group
"""

# Inspired from https://gist.github.com/omegahm/28d87a4e1411c030aa89

import argparse
import logging
import sys
import requests

from json import load, JSONDecodeError

ENDPOINT_GROUP = 'groups'
ENDPOINT_PROJECT = 'projects'

def parser_args():
    """ Build command line parser, parse cmd line and return args

    :return: args
    :rtype: argparse.Namespace
    """
    parser = argparse.ArgumentParser()
    # logging level
    parser.add_argument("-v", "--verbose",
                        action="store_const",
                        dest="loglevel",
                        const=logging.INFO,
                        help="verbose mode")
    parser.add_argument("-d", "--debug",
                        action="store_const",
                        default=logging.WARNING,
                        const=logging.DEBUG,
                        dest="loglevel",
                        help="debug mode")
    parser.add_argument("-q", "--quiet",
                        action="store_const",
                        dest="loglevel",
                        const=logging.ERROR,
                        help="show only ERROR & CRITICAL messages")

    # option to get labels file
    parser.add_argument('-f', '--file',
                        type=argparse.FileType('r'),
                        required=True,
                        help='input file containing data aboout labels to create')

    # option to create label in group
    parser.add_argument("-g", "--group",
                        action="store_true",
                        help="create labels at group level instead of project")

    # option to delete all currents labels
    parser.add_argument("--delete",
                        default=False, action="store_true",
                        help="WARNING: this option deletes all existing labels and exit")

    # args group with gitlab server options
    gitlab_parser = parser.add_argument_group("gitlab",
                                              "options about gitlab server & project")

    gitlab_parser.add_argument("-H", "--host", required=True,
                               help="gitlab server adress")

    gitlab_parser.add_argument("-t", "--token", required=True, help="api token")

    gitlab_parser.add_argument("--id", required=True,
                               help="id of the project/group")

    gitlab_parser.add_argument("--http", default=False,
                               action="store_true",
                               help="this option permit to use HTTP instead of HTTPS to reach gitlab server")
    gitlab_parser.add_argument("-k", "--insecure", default=False,
                               action="store_true",
                               help="this  option  explicitly allows to perform 'insecure' SSL connections and transfers.")
    gitlab_parser.add_argument("-u", "--update",
                               default=False, action="store_true",
                               help="update the label color if its already exists")

    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel, format='%(message)s')
    # Disable warning grom urllib3
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    return args


def main():
    args = parser_args()

    # Build API address
    endpoint = ENDPOINT_PROJECT
    if args.group:
        endpoint = ENDPOINT_GROUP
        logging.info("Labels will be created at GROUP level")
    else:
        logging.info("Labels will be created at PROJECT level")
    protocol = "http" if args.http else "https"
    address = "%s://%s/api/v4/%s/%s/labels" % (protocol, args.host, endpoint, args.id)
    headers = {'PRIVATE-TOKEN': args.token}

    # If needed, delete all labels and exit
    if args.delete:
        logging.info('Get all the current labels ID before deletion')
        r = requests.get(address, headers=headers)
        old_labels = [label['id'] for label in r.json()]
        logging.warning('Current labels will be deleted')
        for old_label in old_labels:
            requests.delete('{}/{}'.format(address, old_label), headers=headers)
            logging.info('Label {} deleted'.format(old_label))
        exit(0)

    # Get labels to create
    labels = dict()
    try:
        labels = load(args.file)
    except JSONDecodeError:
        logging.error('No JSON object could be decoded in "{}"'.format(
                      args.file))
        exit(1)

    # Send creation requests to API
    for label in labels:
        logging.info("Add label %s with color %s", label['name'], label['color'])
        #headers = {'Content-Type': 'application/json', 'PRIVATE-TOKEN': args.token}
        data = {'name': label['name'],
                'description': label['description'],
                'color': "#{}".format(label['color'])}
        logging.debug('    [query] address:%s, data:%s, headers:%s', address, data, headers)
        r = requests.post(address, data=data, headers=headers)
        logging.info("    -> Result: %s %s", r.status_code, r.text)

if __name__ == "__main__":
    main()
